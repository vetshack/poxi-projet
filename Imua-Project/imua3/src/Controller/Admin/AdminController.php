<?php

namespace App\Controller\Admin;

use App\Entity\Civility;

use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return parent::index();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            // the name visible to end users

            ->setTitle('<img src="..."> IMUA <span class="text-small">Corp.</span>');
    }

    //Menu latérale
    public function configureMenuItems(): iterable
    {
        yield MenuItem::section('Gestion associative IMUA');

        yield MenuItem::section('Liste des Adhérents IMUA', 'fa fa-list');
        yield MenuItem::linktoDashboard('Liste des Adhérents Face');

        yield MenuItem::section('Ajout adhérents', 'fa fa-plus');
        yield MenuItem::subMenu('Ajout adhérents Face');
        yield MenuItem::linktoDashboard('Ajout adhérents Papanui');
        yield MenuItem::linktoDashboard('Ajout adhérents Huma');

        yield MenuItem::section('Gestion des utilisateurs', 'fa fa-users');
        yield MenuItem::subMenu('liste des utilisateurs authentifiés');

        yield MenuItem::section('Panneaux de configuration', 'fa fa-tools');
//        yield MenuItem::linkToCrud('civilité', '', Civility::class);
        // yield MenuItem::linkToCrud('The Label', 'fas fa-list', EntityClass::class);
    }
}
