<?php

namespace CivilityController;

use App\Entity\Civility;

use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Contracts\Controller\CrudControllerInterface;

class CivilityCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Civility::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
    public function createEntity(string $entityFqcn)
    {
        $civilityCreated = new Civility();
        $civilityCreated->createdBy($this->getUser());

        return $civilityCreated;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud

            ->setPageTitle('index', '%entity_label_plural% listing');
    }
}
