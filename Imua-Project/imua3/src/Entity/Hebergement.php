<?php

namespace App\Entity;

use App\Repository\HebergementRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=HebergementRepository::class)
 */
class Hebergement
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomTypeHebergement;

    /**
     * @ORM\OneToMany(targetEntity=Members::class, mappedBy="hostBy")
     */
    private $hostMemberType;

    public function __construct()
    {
        $this->hostMemberType = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomTypeHebergement(): ?string
    {
        return $this->nomTypeHebergement;
    }

    public function setNomTypeHebergement(string $nomTypeHebergement): self
    {
        $this->nomTypeHebergement = $nomTypeHebergement;

        return $this;
    }

    /**
     * @return Collection|Members[]
     */
    public function getHostMemberType(): Collection
    {
        return $this->hostMemberType;
    }

    public function addHostMemberType(Members $hostMemberType): self
    {
        if (!$this->hostMemberType->contains($hostMemberType)) {
            $this->hostMemberType[] = $hostMemberType;
            $hostMemberType->setHostBy($this);
        }

        return $this;
    }

    public function removeHostMemberType(Members $hostMemberType): self
    {
        if ($this->hostMemberType->removeElement($hostMemberType)) {
            // set the owning side to null (unless already changed)
            if ($hostMemberType->getHostBy() === $this) {
                $hostMemberType->setHostBy(null);
            }
        }

        return $this;
    }
}
