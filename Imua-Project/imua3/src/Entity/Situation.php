<?php

namespace App\Entity;

use App\Repository\SituationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SituationRepository::class)
 */
class Situation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomTypeSituation;

    /**
     * @ORM\OneToMany(targetEntity=Members::class, mappedBy="legalSituation")
     */
    private $members;

    /**
     * @ORM\OneToMany(targetEntity=Members::class, mappedBy="familySituation")
     */
    private $familySituation;

    public function __construct()
    {
        $this->members = new ArrayCollection();
        $this->familySituation = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomTypeSituation(): ?string
    {
        return $this->nomTypeSituation;
    }

    public function setNomTypeSituation(string $nomTypeSituation): self
    {
        $this->nomTypeSituation = $nomTypeSituation;

        return $this;
    }

    /**
     * @return Collection|Members[]
     */
    public function getMembers(): Collection
    {
        return $this->members;
    }

    public function addMember(Members $member): self
    {
        if (!$this->members->contains($member)) {
            $this->members[] = $member;
            $member->setLegalSituation($this);
        }

        return $this;
    }

    public function removeMember(Members $member): self
    {
        if ($this->members->removeElement($member)) {
            // set the owning side to null (unless already changed)
            if ($member->getLegalSituation() === $this) {
                $member->setLegalSituation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Members[]
     */
    public function getFamilySituation(): Collection
    {
        return $this->familySituation;
    }

    public function addFamilySituation(Members $familySituation): self
    {
        if (!$this->familySituation->contains($familySituation)) {
            $this->familySituation[] = $familySituation;
            $familySituation->setFamilySituation($this);
        }

        return $this;
    }

    public function removeFamilySituation(Members $familySituation): self
    {
        if ($this->familySituation->removeElement($familySituation)) {
            // set the owning side to null (unless already changed)
            if ($familySituation->getFamilySituation() === $this) {
                $familySituation->setFamilySituation(null);
            }
        }

        return $this;
    }
}
