<?php

namespace App\Entity;

use App\Repository\TownRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TownRepository::class)
 */
class Town
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $townName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $townSlug;

    /**
     * @ORM\OneToMany(targetEntity=District::class, mappedBy="town")
     */
    private $district;

    /**
     * @ORM\ManyToOne(targetEntity=Island::class, inversedBy="town")
     */
    private $Island;

    /**
     * @ORM\OneToMany(targetEntity=Members::class, mappedBy="town")
     */
    private $townMember;

    public function __construct()
    {
        $this->district = new ArrayCollection();
        $this->townMember = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTownName(): ?string
    {
        return $this->townName;
    }

    public function setTownName(string $townName): self
    {
        $this->townName = $townName;

        return $this;
    }

    public function getTownSlug(): ?string
    {
        return $this->townSlug;
    }

    public function setTownSlug(?string $townSlug): self
    {
        $this->townSlug = $townSlug;

        return $this;
    }

    /**
     * @return Collection|District[]
     */
    public function getDistrict(): Collection
    {
        return $this->district;
    }

    public function addDistrict(District $district): self
    {
        if (!$this->district->contains($district)) {
            $this->district[] = $district;
            $district->setTown($this);
        }

        return $this;
    }

    public function removeDistrict(District $district): self
    {
        if ($this->district->removeElement($district)) {
            // set the owning side to null (unless already changed)
            if ($district->getTown() === $this) {
                $district->setTown(null);
            }
        }

        return $this;
    }

    public function getIsland(): ?Island
    {
        return $this->Island;
    }

    public function setIsland(?Island $Island): self
    {
        $this->Island = $Island;

        return $this;
    }

    /**
     * @return Collection|Members[]
     */
    public function getTownMember(): Collection
    {
        return $this->townMember;
    }

    public function addTownMember(Members $townMember): self
    {
        if (!$this->townMember->contains($townMember)) {
            $this->townMember[] = $townMember;
            $townMember->setTown($this);
        }

        return $this;
    }

    public function removeTownMember(Members $townMember): self
    {
        if ($this->townMember->removeElement($townMember)) {
            // set the owning side to null (unless already changed)
            if ($townMember->getTown() === $this) {
                $townMember->setTown(null);
            }
        }

        return $this;
    }
}
