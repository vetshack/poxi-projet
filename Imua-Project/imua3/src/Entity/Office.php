<?php

namespace App\Entity;

use App\Repository\OfficeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OfficeRepository::class)
 */
class Office
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $officeName;

    /**
     * @ORM\OneToMany(targetEntity=Members::class, mappedBy="office")
     */
    private $officeMember;

    public function __construct()
    {
        $this->officeMember = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOfficeName(): ?string
    {
        return $this->officeName;
    }

    public function setOfficeName(string $officeName): self
    {
        $this->officeName = $officeName;

        return $this;
    }

    /**
     * @return Collection|Members[]
     */
    public function getOfficeMember(): Collection
    {
        return $this->officeMember;
    }

    public function addOfficeMember(Members $officeMember): self
    {
        if (!$this->officeMember->contains($officeMember)) {
            $this->officeMember[] = $officeMember;
            $officeMember->setOffice($this);
        }

        return $this;
    }

    public function removeOfficeMember(Members $officeMember): self
    {
        if ($this->officeMember->removeElement($officeMember)) {
            // set the owning side to null (unless already changed)
            if ($officeMember->getOffice() === $this) {
                $officeMember->setOffice(null);
            }
        }

        return $this;
    }
}
