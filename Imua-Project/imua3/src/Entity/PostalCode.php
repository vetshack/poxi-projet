<?php

namespace App\Entity;

use App\Repository\PostalCodeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PostalCodeRepository::class)
 */
class PostalCode
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $postalCode;

    /**
     * @ORM\OneToMany(targetEntity=Members::class, mappedBy="postalCode")
     */
    private $postalCodeMember;

    public function __construct()
    {
        $this->postalCodeMember = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPostalCode(): ?int
    {
        return $this->postalCode;
    }

    public function setPostalCode(int $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * @return Collection|Members[]
     */
    public function getPostalCodeMember(): Collection
    {
        return $this->postalCodeMember;
    }

    public function addPostalCodeMember(Members $postalCodeMember): self
    {
        if (!$this->postalCodeMember->contains($postalCodeMember)) {
            $this->postalCodeMember[] = $postalCodeMember;
            $postalCodeMember->setPostalCode($this);
        }

        return $this;
    }

    public function removePostalCodeMember(Members $postalCodeMember): self
    {
        if ($this->postalCodeMember->removeElement($postalCodeMember)) {
            // set the owning side to null (unless already changed)
            if ($postalCodeMember->getPostalCode() === $this) {
                $postalCodeMember->setPostalCode(null);
            }
        }

        return $this;
    }
}
