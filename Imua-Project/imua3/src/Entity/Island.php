<?php

namespace App\Entity;

use App\Repository\IslandRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=IslandRepository::class)
 */
class Island
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $islandName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $islandSlug;

    /**
     * @ORM\OneToMany(targetEntity=Town::class, mappedBy="Island")
     */
    private $town;

    /**
     * @ORM\OneToMany(targetEntity=Members::class, mappedBy="island")
     */
    private $islandMember;

    public function __construct()
    {
        $this->town = new ArrayCollection();
        $this->islandMember = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIslandName(): ?string
    {
        return $this->islandName;
    }

    public function setIslandName(string $islandName): self
    {
        $this->islandName = $islandName;

        return $this;
    }

    public function getIslandSlug(): ?string
    {
        return $this->islandSlug;
    }

    public function setIslandSlug(?string $islandSlug): self
    {
        $this->islandSlug = $islandSlug;

        return $this;
    }

    /**
     * @return Collection|Town[]
     */
    public function getTown(): Collection
    {
        return $this->town;
    }

    public function addTown(Town $town): self
    {
        if (!$this->town->contains($town)) {
            $this->town[] = $town;
            $town->setIsland($this);
        }

        return $this;
    }

    public function removeTown(Town $town): self
    {
        if ($this->town->removeElement($town)) {
            // set the owning side to null (unless already changed)
            if ($town->getIsland() === $this) {
                $town->setIsland(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Members[]
     */
    public function getIslandMember(): Collection
    {
        return $this->islandMember;
    }

    public function addIslandMember(Members $islandMember): self
    {
        if (!$this->islandMember->contains($islandMember)) {
            $this->islandMember[] = $islandMember;
            $islandMember->setIsland($this);
        }

        return $this;
    }

    public function removeIslandMember(Members $islandMember): self
    {
        if ($this->islandMember->removeElement($islandMember)) {
            // set the owning side to null (unless already changed)
            if ($islandMember->getIsland() === $this) {
                $islandMember->setIsland(null);
            }
        }

        return $this;
    }
}
