<?php

namespace App\Entity;

use App\Repository\DistrictRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DistrictRepository::class)
 */
class District
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $districtName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $districtSlug;

    /**
     * @ORM\ManyToOne(targetEntity=Town::class, inversedBy="district")
     */
    private $town;

    /**
     * @ORM\OneToMany(targetEntity=Members::class, mappedBy="district")
     */
    private $districtMember;

    public function __construct()
    {
        $this->districtMember = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDistrictName(): ?string
    {
        return $this->districtName;
    }

    public function setDistrictName(string $districtName): self
    {
        $this->districtName = $districtName;

        return $this;
    }

    public function getDistrictSlug(): ?string
    {
        return $this->districtSlug;
    }

    public function setDistrictSlug(?string $districtSlug): self
    {
        $this->districtSlug = $districtSlug;

        return $this;
    }

    public function getTown(): ?Town
    {
        return $this->town;
    }

    public function setTown(?Town $town): self
    {
        $this->town = $town;

        return $this;
    }

    /**
     * @return Collection|Members[]
     */
    public function getDistrictMember(): Collection
    {
        return $this->districtMember;
    }

    public function addDistrictMember(Members $districtMember): self
    {
        if (!$this->districtMember->contains($districtMember)) {
            $this->districtMember[] = $districtMember;
            $districtMember->setDistrict($this);
        }

        return $this;
    }

    public function removeDistrictMember(Members $districtMember): self
    {
        if ($this->districtMember->removeElement($districtMember)) {
            // set the owning side to null (unless already changed)
            if ($districtMember->getDistrict() === $this) {
                $districtMember->setDistrict(null);
            }
        }

        return $this;
    }
}
