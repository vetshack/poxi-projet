<?php

namespace App\Entity;

use App\Repository\MembersRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MembersRepository::class)
 */
class Members
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $dn_number;

    /**
     * @ORM\Column(type="integer")
     */
    private $optIn_number;

    /**
     * @ORM\Column(type="datetime")
     */
    private $optIn_date;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateRecordCreated;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateRecordUpdated;

    /**
     * @ORM\Column(type="boolean")
     */
    private $identityConfirmed;

    /**
     * @ORM\ManyToOne(targetEntity=Civility::class, inversedBy="members")
     * @ORM\JoinColumn(nullable=false)
     */
    private $typeCivility;

    /**
     * @ORM\ManyToOne(targetEntity=Gender::class, inversedBy="members")
     * @ORM\JoinColumn(nullable=false)
     */
    private $typeGender;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nickname;

    /**
     * @ORM\ManyToOne(targetEntity=Situation::class, inversedBy="members")
     * @ORM\JoinColumn(nullable=false)
     */
    private $legalSituation;

    /**
     * @ORM\ManyToOne(targetEntity=Situation::class, inversedBy="familySituation")
     * @ORM\JoinColumn(nullable=false)
     */
    private $familySituation;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $childrenNumber;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $numberChildrenUnderSameRoof;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $numberPartTimeChildren;

    /**
     * @ORM\Column(type="date")
     */
    private $birthDay;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $birthTown;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $birthCountry;

    /**
     * @ORM\ManyToOne(targetEntity=Hebergement::class, inversedBy="hostMemberType")
     */
    private $hostBy;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Address;

    /**
     * @ORM\ManyToOne(targetEntity=Island::class, inversedBy="islandMember")
     */
    private $island;

    /**
     * @ORM\ManyToOne(targetEntity=Town::class, inversedBy="townMember")
     */
    private $town;

    /**
     * @ORM\ManyToOne(targetEntity=District::class, inversedBy="districtMember")
     */
    private $district;

    /**
     * @ORM\ManyToOne(targetEntity=PostalCode::class, inversedBy="postalCodeMember")
     */
    private $postalCode;

    /**
     * @ORM\ManyToOne(targetEntity=Office::class, inversedBy="officeMember")
     */
    private $office;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDnNumber(): ?int
    {
        return $this->dn_number;
    }

    public function setDnNumber(int $dn_number): self
    {
        $this->dn_number = $dn_number;

        return $this;
    }

    public function getOptInNumber(): ?int
    {
        return $this->optIn_number;
    }

    public function setOptInNumber(int $optIn_number): self
    {
        $this->optIn_number = $optIn_number;

        return $this;
    }

    public function getOptInDate(): ?\DateTimeInterface
    {
        return $this->optIn_date;
    }

    public function setOptInDate(\DateTimeInterface $optIn_date): self
    {
        $this->optIn_date = $optIn_date;

        return $this;
    }

    public function getDateRecordCreated(): ?\DateTimeInterface
    {
        return $this->dateRecordCreated;
    }

    public function setDateRecordCreated(\DateTimeInterface $dateRecordCreated): self
    {
        $this->dateRecordCreated = $dateRecordCreated;

        return $this;
    }

    public function getDateRecordUpdated(): ?\DateTimeInterface
    {
        return $this->dateRecordUpdated;
    }

    public function setDateRecordUpdated(?\DateTimeInterface $dateRecordUpdated): self
    {
        $this->dateRecordUpdated = $dateRecordUpdated;

        return $this;
    }

    public function getIdentityConfirmed(): ?bool
    {
        return $this->identityConfirmed;
    }

    public function setIdentityConfirmed(bool $identityConfirmed): self
    {
        $this->identityConfirmed = $identityConfirmed;

        return $this;
    }

    public function getTypeCivility(): ?Civility
    {
        return $this->typeCivility;
    }

    public function setTypeCivility(?Civility $typeCivility): self
    {
        $this->typeCivility = $typeCivility;

        return $this;
    }

    public function getTypeGender(): ?Gender
    {
        return $this->typeGender;
    }

    public function setTypeGender(?Gender $typeGender): self
    {
        $this->typeGender = $typeGender;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    public function setNickname(string $nickname): self
    {
        $this->nickname = $nickname;

        return $this;
    }

    public function getLegalSituation(): ?Situation
    {
        return $this->legalSituation;
    }

    public function setLegalSituation(?Situation $legalSituation): self
    {
        $this->legalSituation = $legalSituation;

        return $this;
    }

    public function getFamilySituation(): ?Situation
    {
        return $this->familySituation;
    }

    public function setFamilySituation(?Situation $familySituation): self
    {
        $this->familySituation = $familySituation;

        return $this;
    }

    public function getChildrenNumber(): ?int
    {
        return $this->childrenNumber;
    }

    public function setChildrenNumber(?int $childrenNumber): self
    {
        $this->childrenNumber = $childrenNumber;

        return $this;
    }

    public function getNumberChildrenUnderSameRoof(): ?int
    {
        return $this->numberChildrenUnderSameRoof;
    }

    public function setNumberChildrenUnderSameRoof(?int $numberChildrenUnderSameRoof): self
    {
        $this->numberChildrenUnderSameRoof = $numberChildrenUnderSameRoof;

        return $this;
    }

    public function getNumberPartTimeChildren(): ?int
    {
        return $this->numberPartTimeChildren;
    }

    public function setNumberPartTimeChildren(?int $numberPartTimeChildren): self
    {
        $this->numberPartTimeChildren = $numberPartTimeChildren;

        return $this;
    }

    public function getBirthDay(): ?\DateTimeInterface
    {
        return $this->birthDay;
    }

    public function setBirthDay(\DateTimeInterface $birthDay): self
    {
        $this->birthDay = $birthDay;

        return $this;
    }

    public function getBirthTown(): ?string
    {
        return $this->birthTown;
    }

    public function setBirthTown(string $birthTown): self
    {
        $this->birthTown = $birthTown;

        return $this;
    }

    public function getBirthCountry(): ?string
    {
        return $this->birthCountry;
    }

    public function setBirthCountry(string $birthCountry): self
    {
        $this->birthCountry = $birthCountry;

        return $this;
    }

    public function getHostBy(): ?Hebergement
    {
        return $this->hostBy;
    }

    public function setHostBy(?Hebergement $hostBy): self
    {
        $this->hostBy = $hostBy;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->Address;
    }

    public function setAddress(?string $Address): self
    {
        $this->Address = $Address;

        return $this;
    }

    public function getIsland(): ?Island
    {
        return $this->island;
    }

    public function setIsland(?Island $island): self
    {
        $this->island = $island;

        return $this;
    }

    public function getTown(): ?Town
    {
        return $this->town;
    }

    public function setTown(?Town $town): self
    {
        $this->town = $town;

        return $this;
    }

    public function getDistrict(): ?District
    {
        return $this->district;
    }

    public function setDistrict(?District $district): self
    {
        $this->district = $district;

        return $this;
    }

    public function getPostalCode(): ?PostalCode
    {
        return $this->postalCode;
    }

    public function setPostalCode(?PostalCode $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getOffice(): ?Office
    {
        return $this->office;
    }

    public function setOffice(?Office $office): self
    {
        $this->office = $office;

        return $this;
    }
}
