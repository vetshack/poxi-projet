<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210622001730 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE office (id INT AUTO_INCREMENT NOT NULL, office_name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE members ADD type_civility_id INT NOT NULL, ADD type_gender_id INT NOT NULL, ADD legal_situation_id INT NOT NULL, ADD family_situation_id INT NOT NULL, ADD last_name VARCHAR(255) NOT NULL, ADD nickname VARCHAR(255) NOT NULL, ADD children_number INT DEFAULT NULL, ADD number_children_under_same_roof INT DEFAULT NULL, ADD number_part_time_children INT DEFAULT NULL, ADD birth_day DATE NOT NULL');
        $this->addSql('ALTER TABLE members ADD CONSTRAINT FK_45A0D2FF68CF7509 FOREIGN KEY (type_civility_id) REFERENCES civility (id)');
        $this->addSql('ALTER TABLE members ADD CONSTRAINT FK_45A0D2FFB3CEE1F0 FOREIGN KEY (type_gender_id) REFERENCES gender (id)');
        $this->addSql('ALTER TABLE members ADD CONSTRAINT FK_45A0D2FFE310FCEC FOREIGN KEY (legal_situation_id) REFERENCES situation (id)');
        $this->addSql('ALTER TABLE members ADD CONSTRAINT FK_45A0D2FF83753D32 FOREIGN KEY (family_situation_id) REFERENCES situation (id)');
        $this->addSql('CREATE INDEX IDX_45A0D2FF68CF7509 ON members (type_civility_id)');
        $this->addSql('CREATE INDEX IDX_45A0D2FFB3CEE1F0 ON members (type_gender_id)');
        $this->addSql('CREATE INDEX IDX_45A0D2FFE310FCEC ON members (legal_situation_id)');
        $this->addSql('CREATE INDEX IDX_45A0D2FF83753D32 ON members (family_situation_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE office');
        $this->addSql('ALTER TABLE members DROP FOREIGN KEY FK_45A0D2FF68CF7509');
        $this->addSql('ALTER TABLE members DROP FOREIGN KEY FK_45A0D2FFB3CEE1F0');
        $this->addSql('ALTER TABLE members DROP FOREIGN KEY FK_45A0D2FFE310FCEC');
        $this->addSql('ALTER TABLE members DROP FOREIGN KEY FK_45A0D2FF83753D32');
        $this->addSql('DROP INDEX IDX_45A0D2FF68CF7509 ON members');
        $this->addSql('DROP INDEX IDX_45A0D2FFB3CEE1F0 ON members');
        $this->addSql('DROP INDEX IDX_45A0D2FFE310FCEC ON members');
        $this->addSql('DROP INDEX IDX_45A0D2FF83753D32 ON members');
        $this->addSql('ALTER TABLE members DROP type_civility_id, DROP type_gender_id, DROP legal_situation_id, DROP family_situation_id, DROP last_name, DROP nickname, DROP children_number, DROP number_children_under_same_roof, DROP number_part_time_children, DROP birth_day');
    }
}
