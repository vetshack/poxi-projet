<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210622011838 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE postal_code (id INT AUTO_INCREMENT NOT NULL, postal_code INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE members ADD host_by_id INT DEFAULT NULL, ADD island_id INT DEFAULT NULL, ADD town_id INT DEFAULT NULL, ADD district_id INT DEFAULT NULL, ADD postal_code_id INT DEFAULT NULL, ADD office_id INT DEFAULT NULL, ADD birth_town VARCHAR(255) NOT NULL, ADD birth_country VARCHAR(255) NOT NULL, ADD address VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE members ADD CONSTRAINT FK_45A0D2FF9856DF7 FOREIGN KEY (host_by_id) REFERENCES hebergement (id)');
        $this->addSql('ALTER TABLE members ADD CONSTRAINT FK_45A0D2FFB0EEFA16 FOREIGN KEY (island_id) REFERENCES island (id)');
        $this->addSql('ALTER TABLE members ADD CONSTRAINT FK_45A0D2FF75E23604 FOREIGN KEY (town_id) REFERENCES town (id)');
        $this->addSql('ALTER TABLE members ADD CONSTRAINT FK_45A0D2FFB08FA272 FOREIGN KEY (district_id) REFERENCES district (id)');
        $this->addSql('ALTER TABLE members ADD CONSTRAINT FK_45A0D2FFBDBA6A61 FOREIGN KEY (postal_code_id) REFERENCES postal_code (id)');
        $this->addSql('ALTER TABLE members ADD CONSTRAINT FK_45A0D2FFFFA0C224 FOREIGN KEY (office_id) REFERENCES office (id)');
        $this->addSql('CREATE INDEX IDX_45A0D2FF9856DF7 ON members (host_by_id)');
        $this->addSql('CREATE INDEX IDX_45A0D2FFB0EEFA16 ON members (island_id)');
        $this->addSql('CREATE INDEX IDX_45A0D2FF75E23604 ON members (town_id)');
        $this->addSql('CREATE INDEX IDX_45A0D2FFB08FA272 ON members (district_id)');
        $this->addSql('CREATE INDEX IDX_45A0D2FFBDBA6A61 ON members (postal_code_id)');
        $this->addSql('CREATE INDEX IDX_45A0D2FFFFA0C224 ON members (office_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE members DROP FOREIGN KEY FK_45A0D2FFBDBA6A61');
        $this->addSql('DROP TABLE postal_code');
        $this->addSql('ALTER TABLE members DROP FOREIGN KEY FK_45A0D2FF9856DF7');
        $this->addSql('ALTER TABLE members DROP FOREIGN KEY FK_45A0D2FFB0EEFA16');
        $this->addSql('ALTER TABLE members DROP FOREIGN KEY FK_45A0D2FF75E23604');
        $this->addSql('ALTER TABLE members DROP FOREIGN KEY FK_45A0D2FFB08FA272');
        $this->addSql('ALTER TABLE members DROP FOREIGN KEY FK_45A0D2FFFFA0C224');
        $this->addSql('DROP INDEX IDX_45A0D2FF9856DF7 ON members');
        $this->addSql('DROP INDEX IDX_45A0D2FFB0EEFA16 ON members');
        $this->addSql('DROP INDEX IDX_45A0D2FF75E23604 ON members');
        $this->addSql('DROP INDEX IDX_45A0D2FFB08FA272 ON members');
        $this->addSql('DROP INDEX IDX_45A0D2FFBDBA6A61 ON members');
        $this->addSql('DROP INDEX IDX_45A0D2FFFFA0C224 ON members');
        $this->addSql('ALTER TABLE members DROP host_by_id, DROP island_id, DROP town_id, DROP district_id, DROP postal_code_id, DROP office_id, DROP birth_town, DROP birth_country, DROP address');
    }
}
