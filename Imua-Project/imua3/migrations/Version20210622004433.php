<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210622004433 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE district ADD town_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE district ADD CONSTRAINT FK_31C1548775E23604 FOREIGN KEY (town_id) REFERENCES town (id)');
        $this->addSql('CREATE INDEX IDX_31C1548775E23604 ON district (town_id)');
        $this->addSql('ALTER TABLE town ADD island_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE town ADD CONSTRAINT FK_4CE6C7A4B0EEFA16 FOREIGN KEY (island_id) REFERENCES island (id)');
        $this->addSql('CREATE INDEX IDX_4CE6C7A4B0EEFA16 ON town (island_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE district DROP FOREIGN KEY FK_31C1548775E23604');
        $this->addSql('DROP INDEX IDX_31C1548775E23604 ON district');
        $this->addSql('ALTER TABLE district DROP town_id');
        $this->addSql('ALTER TABLE town DROP FOREIGN KEY FK_4CE6C7A4B0EEFA16');
        $this->addSql('DROP INDEX IDX_4CE6C7A4B0EEFA16 ON town');
        $this->addSql('ALTER TABLE town DROP island_id');
    }
}
