<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210622194537 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE civility (id INT AUTO_INCREMENT NOT NULL, nom_type_civility VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE country (id INT AUTO_INCREMENT NOT NULL, code INT NOT NULL, alpha2 VARCHAR(2) NOT NULL, alpha3 VARCHAR(3) NOT NULL, name_en_gb VARCHAR(60) NOT NULL, name_fr_fr VARCHAR(60) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE district (id INT AUTO_INCREMENT NOT NULL, town_id INT DEFAULT NULL, district_name VARCHAR(255) NOT NULL, district_slug VARCHAR(255) DEFAULT NULL, INDEX IDX_31C1548775E23604 (town_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE gender (id INT AUTO_INCREMENT NOT NULL, nom_type_gender VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE hebergement (id INT AUTO_INCREMENT NOT NULL, nom_type_hebergement VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE island (id INT AUTO_INCREMENT NOT NULL, island_name VARCHAR(255) NOT NULL, island_slug VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE members (id INT AUTO_INCREMENT NOT NULL, type_civility_id INT NOT NULL, type_gender_id INT NOT NULL, legal_situation_id INT NOT NULL, family_situation_id INT NOT NULL, host_by_id INT DEFAULT NULL, island_id INT DEFAULT NULL, town_id INT DEFAULT NULL, district_id INT DEFAULT NULL, postal_code_id INT DEFAULT NULL, office_id INT DEFAULT NULL, dn_number INT NOT NULL, opt_in_number INT NOT NULL, opt_in_date DATETIME NOT NULL, date_record_created DATETIME NOT NULL, date_record_updated DATETIME DEFAULT NULL, identity_confirmed TINYINT(1) NOT NULL, last_name VARCHAR(255) NOT NULL, nickname VARCHAR(255) NOT NULL, children_number INT DEFAULT NULL, number_children_under_same_roof INT DEFAULT NULL, number_part_time_children INT DEFAULT NULL, birth_day DATE NOT NULL, birth_town VARCHAR(255) NOT NULL, birth_country VARCHAR(255) NOT NULL, address VARCHAR(255) DEFAULT NULL, INDEX IDX_45A0D2FF68CF7509 (type_civility_id), INDEX IDX_45A0D2FFB3CEE1F0 (type_gender_id), INDEX IDX_45A0D2FFE310FCEC (legal_situation_id), INDEX IDX_45A0D2FF83753D32 (family_situation_id), INDEX IDX_45A0D2FF9856DF7 (host_by_id), INDEX IDX_45A0D2FFB0EEFA16 (island_id), INDEX IDX_45A0D2FF75E23604 (town_id), INDEX IDX_45A0D2FFB08FA272 (district_id), INDEX IDX_45A0D2FFBDBA6A61 (postal_code_id), INDEX IDX_45A0D2FFFFA0C224 (office_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE office (id INT AUTO_INCREMENT NOT NULL, office_name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE postal_code (id INT AUTO_INCREMENT NOT NULL, postal_code INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE situation (id INT AUTO_INCREMENT NOT NULL, nom_type_situation VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE town (id INT AUTO_INCREMENT NOT NULL, island_id INT DEFAULT NULL, town_name VARCHAR(255) NOT NULL, town_slug VARCHAR(255) DEFAULT NULL, INDEX IDX_4CE6C7A4B0EEFA16 (island_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE district ADD CONSTRAINT FK_31C1548775E23604 FOREIGN KEY (town_id) REFERENCES town (id)');
        $this->addSql('ALTER TABLE members ADD CONSTRAINT FK_45A0D2FF68CF7509 FOREIGN KEY (type_civility_id) REFERENCES civility (id)');
        $this->addSql('ALTER TABLE members ADD CONSTRAINT FK_45A0D2FFB3CEE1F0 FOREIGN KEY (type_gender_id) REFERENCES gender (id)');
        $this->addSql('ALTER TABLE members ADD CONSTRAINT FK_45A0D2FFE310FCEC FOREIGN KEY (legal_situation_id) REFERENCES situation (id)');
        $this->addSql('ALTER TABLE members ADD CONSTRAINT FK_45A0D2FF83753D32 FOREIGN KEY (family_situation_id) REFERENCES situation (id)');
        $this->addSql('ALTER TABLE members ADD CONSTRAINT FK_45A0D2FF9856DF7 FOREIGN KEY (host_by_id) REFERENCES hebergement (id)');
        $this->addSql('ALTER TABLE members ADD CONSTRAINT FK_45A0D2FFB0EEFA16 FOREIGN KEY (island_id) REFERENCES island (id)');
        $this->addSql('ALTER TABLE members ADD CONSTRAINT FK_45A0D2FF75E23604 FOREIGN KEY (town_id) REFERENCES town (id)');
        $this->addSql('ALTER TABLE members ADD CONSTRAINT FK_45A0D2FFB08FA272 FOREIGN KEY (district_id) REFERENCES district (id)');
        $this->addSql('ALTER TABLE members ADD CONSTRAINT FK_45A0D2FFBDBA6A61 FOREIGN KEY (postal_code_id) REFERENCES postal_code (id)');
        $this->addSql('ALTER TABLE members ADD CONSTRAINT FK_45A0D2FFFFA0C224 FOREIGN KEY (office_id) REFERENCES office (id)');
        $this->addSql('ALTER TABLE town ADD CONSTRAINT FK_4CE6C7A4B0EEFA16 FOREIGN KEY (island_id) REFERENCES island (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE members DROP FOREIGN KEY FK_45A0D2FF68CF7509');
        $this->addSql('ALTER TABLE members DROP FOREIGN KEY FK_45A0D2FFB08FA272');
        $this->addSql('ALTER TABLE members DROP FOREIGN KEY FK_45A0D2FFB3CEE1F0');
        $this->addSql('ALTER TABLE members DROP FOREIGN KEY FK_45A0D2FF9856DF7');
        $this->addSql('ALTER TABLE members DROP FOREIGN KEY FK_45A0D2FFB0EEFA16');
        $this->addSql('ALTER TABLE town DROP FOREIGN KEY FK_4CE6C7A4B0EEFA16');
        $this->addSql('ALTER TABLE members DROP FOREIGN KEY FK_45A0D2FFFFA0C224');
        $this->addSql('ALTER TABLE members DROP FOREIGN KEY FK_45A0D2FFBDBA6A61');
        $this->addSql('ALTER TABLE members DROP FOREIGN KEY FK_45A0D2FFE310FCEC');
        $this->addSql('ALTER TABLE members DROP FOREIGN KEY FK_45A0D2FF83753D32');
        $this->addSql('ALTER TABLE district DROP FOREIGN KEY FK_31C1548775E23604');
        $this->addSql('ALTER TABLE members DROP FOREIGN KEY FK_45A0D2FF75E23604');
        $this->addSql('DROP TABLE civility');
        $this->addSql('DROP TABLE country');
        $this->addSql('DROP TABLE district');
        $this->addSql('DROP TABLE gender');
        $this->addSql('DROP TABLE hebergement');
        $this->addSql('DROP TABLE island');
        $this->addSql('DROP TABLE members');
        $this->addSql('DROP TABLE office');
        $this->addSql('DROP TABLE postal_code');
        $this->addSql('DROP TABLE situation');
        $this->addSql('DROP TABLE town');
    }
}
